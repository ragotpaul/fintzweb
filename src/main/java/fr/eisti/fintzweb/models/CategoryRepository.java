package fr.eisti.fintzweb.models;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Transactional
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
