package fr.eisti.fintzweb.models;

import javax.persistence.*;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Entity
@Table(name = "votes", uniqueConstraints = @UniqueConstraint(columnNames = {"category_id", "voter_id"}))
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voter_id")
    private User voter;

    public Vote() {
    }

    public Vote(Candidate candidate, Category category, User voter) {
        this.candidate = candidate;
        this.category = category;
        this.voter = voter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getVoter() {
        return voter;
    }

    public void setVoter(User voter) {
        this.voter = voter;
    }

}
