package fr.eisti.fintzweb.models;

import javax.persistence.*;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "candidate_type")
@Table(name = "candidates")
public abstract class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Candidate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
