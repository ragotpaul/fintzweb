package fr.eisti.fintzweb.models;

import javax.transaction.Transactional;

/**
 * Created by ragotpaul on 2/27/17.
 */

@Transactional
public interface CandidateRepository extends CandidateBaseRepository<Candidate> {
}
