package fr.eisti.fintzweb.models;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by ragotpaul on 2/27/17.
 */

@Transactional
public interface UserRepository extends CandidateBaseRepository<User> {

    List<User> findAllByOrderByLastnameAsc();

    User findByEmployeeNumber(Integer employeeNumber);

    User findByUsername(String username);

}
