package fr.eisti.fintzweb.models;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by ragotpaul on 2/27/17.
 */

@Transactional
public interface VoteRepository extends CrudRepository<Vote, Long> {

    List<Vote> findByCategory(Category category);

    @Modifying
    @Query("update Vote v set v.candidate = ?1 where v.category = ?2 and v.voter = ?3")
    int setFixedCandidate(Candidate candidate, Category category, User voter);

}
