package fr.eisti.fintzweb.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by ragotpaul on 2/27/17.
 */

@NoRepositoryBean
public interface CandidateBaseRepository<T extends Candidate> extends CrudRepository<T, Long> {
}
