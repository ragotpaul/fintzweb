package fr.eisti.fintzweb.models;

import javax.persistence.*;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Entity
@DiscriminatorValue(CandidateType.ASSOCIATION)
@Table(name = "associations")
public class Association extends Candidate {

    @Basic
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    public Association() {
    }

    public Association(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
