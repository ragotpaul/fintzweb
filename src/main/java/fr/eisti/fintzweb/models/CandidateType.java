package fr.eisti.fintzweb.models;

/**
 * Created by ragotpaul on 2/27/17.
 */
public final class CandidateType {

    public static final String USER = "user";
    public static final String FAMILY = "family";
    public static final String ASSOCIATION = "association";

    private CandidateType() {
    }

    public static String getUSER() {
        return USER;
    }

    public static String getFAMILY() {
        return FAMILY;
    }

    public static String getASSOCIATION() {
        return ASSOCIATION;
    }

    public static String get(String type) {
        String str;
        switch (type) {
            case "user":
                str = CandidateType.USER;
                break;
            case "family":
                str = CandidateType.FAMILY;
                break;
            case "association":
                str = CandidateType.ASSOCIATION;
                break;
            default:
                str = null;
                break;
        }
        return str;
    }

}
