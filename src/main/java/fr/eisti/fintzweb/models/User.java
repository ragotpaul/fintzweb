package fr.eisti.fintzweb.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Entity
@DiscriminatorValue(CandidateType.USER)
@Table(name = "users")
public class User extends Candidate {

    @Basic
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Basic
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Basic
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Basic
    @Column(name = "promotion")
    private String promotion;

    @Basic
    @Column(name = "employeeNumber", nullable = false, unique = true)
    private Integer employeeNumber;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "voter", cascade = CascadeType.ALL)
    private List<Vote> votes;

    public User() {
    }

    public User(String firstname, String lastname, String username, String promotion, Integer employeeNumber, List<Vote> votes) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.promotion = promotion;
        this.employeeNumber = employeeNumber;
        this.votes = votes;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public Integer getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(Integer employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

}
