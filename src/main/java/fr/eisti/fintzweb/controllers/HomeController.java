package fr.eisti.fintzweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Controller
public class HomeController {

    @RequestMapping("/")
    public ModelAndView index() throws Exception {
        ModelAndView view = new ModelAndView("redirect:/vote");
        return view;
    }

}
