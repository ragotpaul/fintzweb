package fr.eisti.fintzweb.controllers;

import au.com.bytecode.opencsv.CSVReader;
import fr.eisti.fintzweb.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.*;
import java.util.Map;

/**
 * Created by ragotpaul on 2/27/17.
 */

@Controller
public class AdminController {

    @Autowired
    CandidateRepository candidateRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    AssociationRepository associationRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    VoteRepository voteRepository;


    @RequestMapping("/admin")
    public ModelAndView admin() {
        ModelAndView view = new ModelAndView("admin");
        return view;
    }

    @GetMapping("/admin/import")
    public ModelAndView importUsers() {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("import", true);
        return view;
    }

    @PostMapping("/admin/import")
    public
    @ResponseBody
    ModelAndView importUsers(@RequestParam MultipartFile file) {
        File serverFile = null;
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");
                if (!dir.exists())
                    dir.mkdirs();
                serverFile = new File(dir.getAbsolutePath() + File.separator + file.getName());
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (serverFile != null) {
            CSVReader reader = null;
            try {
                reader = new CSVReader(new FileReader(serverFile));
                reader.readNext();
                String[] line;
                while ((line = reader.readNext()) != null) {
                    userRepository.save(new User(line[1], line[2], line[0], line[3], Integer.parseInt(line[4]), null));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return adminManageUsers();
    }

    // LIST ALL

    @RequestMapping("/admin/manage/user")
    public ModelAndView adminManageUsers() {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("list", true);
        view.addObject("user", true);
        view.addObject("users", userRepository.findAllByOrderByLastnameAsc());
        return view;
    }

    @RequestMapping("/admin/manage/family")
    public ModelAndView adminManageFamilies() {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("list", true);
        view.addObject("family", true);
        view.addObject("families", familyRepository.findAll());
        return view;
    }

    @RequestMapping("/admin/manage/association")
    public ModelAndView adminManageAssociations() {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("list", true);
        view.addObject("association", true);
        view.addObject("associations", associationRepository.findAll());
        return view;
    }

    @RequestMapping("/admin/manage/category")
    public ModelAndView adminManageCategories() {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("list", true);
        view.addObject("category", true);
        view.addObject("categories", categoryRepository.findAll());
        return view;
    }

    // MANAGE BY ID

    @RequestMapping("/admin/manage/user/{id}")
    public ModelAndView adminManageUser(@PathVariable String id) {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("edit", true);
        view.addObject("user", true);
        view.addObject("obj", userRepository.findOne(Long.parseLong(id)));
        return view;
    }

    @RequestMapping("/admin/manage/family/{id}")
    public ModelAndView adminManageFamily(@PathVariable String id) {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("edit", true);
        view.addObject("family", true);
        view.addObject("obj", familyRepository.findOne(Long.parseLong(id)));
        return view;
    }

    @RequestMapping("/admin/manage/association/{id}")
    public ModelAndView adminManageAssociation(@PathVariable String id) {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("edit", true);
        view.addObject("association", true);
        view.addObject("obj", associationRepository.findOne(Long.parseLong(id)));
        return view;
    }

    @RequestMapping("/admin/manage/category/{id}")
    public ModelAndView adminManageCategory(@PathVariable String id) {
        ModelAndView view = new ModelAndView("admin");
        view.addObject("edit", true);
        view.addObject("category", true);
        view.addObject("obj", categoryRepository.findOne(Long.parseLong(id)));
        return view;
    }

    // CRUD

    @PostMapping("/admin/create/user")
    public ModelAndView adminCreateUser(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/user");
        if (params.get("firstname") != null &&
                params.get("firstname").length() > 0 &&
                params.get("lastname") != null &&
                params.get("lastname").length() > 0 &&
                params.get("username") != null &&
                params.get("username").length() > 0 &&
                params.get("promotion") != null &&
                params.get("promotion").length() > 0 &&
                params.get("employeeNumber") != null &&
                params.get("employeeNumber").length() > 0) {
            User user = new User(params.get("firstname"),
                    params.get("lastname"),
                    params.get("username"),
                    params.get("promotion"),
                    Integer.parseInt(params.get("employeeNumber")),
                    null);
            userRepository.save(user);
        }
        return view;
    }

    @PostMapping("/admin/create/family")
    public ModelAndView adminCreateFamily(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/family");
        if (params.get("name") != null && params.get("name").length() > 0) {
            Family family = new Family(params.get("name"));
            familyRepository.save(family);
        }
        return view;
    }

    @PostMapping("/admin/create/association")
    public ModelAndView adminCreateAssociation(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/association");
        if (params.get("name") != null && params.get("name").length() > 0) {
            Association association = new Association(params.get("name"));
            associationRepository.save(association);
        }
        return view;
    }

    @PostMapping("/admin/create/category")
    public ModelAndView adminCreateCategory(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/category");
        String type = null;
        if (params.get("type") != null) {
            type = CandidateType.get(params.get("type"));
        }
        if (params.get("name") != null && params.get("name").length() > 0 && type != null) {
            Category category = new Category(params.get("name"), type);
            categoryRepository.save(category);
        }
        return view;
    }

    @PostMapping("/admin/read/user")
    public ModelAndView adminReadUser(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("admin");

        return view;
    }

    @PostMapping("/admin/read/family")
    public ModelAndView adminReadFamily(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("admin");

        return view;
    }

    @PostMapping("/admin/read/association")
    public ModelAndView adminReadAssociation(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("admin");

        return view;
    }

    @PostMapping("/admin/read/category")
    public ModelAndView adminReadCategory(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("admin");

        return view;
    }

    @PostMapping("/admin/update/user")
    public ModelAndView adminUpdateUser(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/user");
        if (params.get("id") != null &&
                params.get("firstname") != null ||
                params.get("firstname").length() > 0 ||
                params.get("lastname") != null ||
                params.get("lastname").length() > 0 ||
                params.get("username") != null ||
                params.get("username").length() > 0 ||
                params.get("promotion") != null ||
                params.get("promotion").length() > 0 ||
                params.get("employeeNumber") != null ||
                params.get("employeeNumber").length() > 0) {
            User user = userRepository.findOne(Long.parseLong(params.get("id")));
            if (params.get("firstname") != null)
                user.setFirstname(params.get("firstname"));
            if (params.get("lastname") != null)
                user.setLastname(params.get("lastname"));
            if (params.get("username") != null)
                user.setUsername(params.get("username"));
            if (params.get("promotion") != null)
                user.setPromotion(params.get("promotion"));
            if (params.get("employeeNumber") != null)
                user.setEmployeeNumber(Integer.parseInt(params.get("employeeNumber")));
            userRepository.save(user);
        }
        return view;
    }

    @PostMapping("/admin/update/family")
    public ModelAndView adminUpdateFamily(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/family");
        if (params.get("id") != null && params.get("name") != null && params.get("name").length() > 0) {
            Family family = (Family) familyRepository.findOne(Long.parseLong(params.get("id")));
            family.setName(params.get("name"));
            candidateRepository.save(family);
        }
        return view;
    }

    @PostMapping("/admin/update/association")
    public ModelAndView adminUpdateAssociation(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/association");
        if (params.get("id") != null && params.get("name") != null && params.get("name").length() > 0) {
            Association association = (Association) associationRepository.findOne(Long.parseLong(params.get("id")));
            association.setName(params.get("name"));
            candidateRepository.save(association);
        }
        return view;
    }

    @PostMapping("/admin/update/category")
    public ModelAndView adminUpdateCategory(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/category");
        String type = null;
        if (params.get("type") != null) {
            type = CandidateType.get(params.get("type"));
        }
        if (params.get("id") != null && params.get("name") != null && params.get("name").length() > 0 && type != null) {
            Category category = categoryRepository.findOne(Long.parseLong(params.get("id")));
            category.setName(params.get("name"));
            if (type != null) {
                category.setCandidateType(type);
            }
            categoryRepository.save(category);
        }
        return view;
    }

    @PostMapping("/admin/delete/user")
    public ModelAndView adminDeleteUser(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/user");
        if (params.get("id") != null) {
            userRepository.delete(Long.parseLong(params.get("id")));
        }
        return view;
    }

    @PostMapping("/admin/delete/family")
    public ModelAndView adminDeleteFamily(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/family");
        if (params.get("id") != null) {
            familyRepository.delete(Long.parseLong(params.get("id")));
        }
        return view;
    }

    @PostMapping("/admin/delete/association")
    public ModelAndView adminDeleteAssociation(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/association");
        if (params.get("id") != null) {
            associationRepository.delete(Long.parseLong(params.get("id")));
        }
        return view;
    }

    @PostMapping("/admin/delete/category")
    public ModelAndView adminDeleteCategory(@RequestParam Map<String, String> params) {
        ModelAndView view = new ModelAndView("redirect:/admin/manage/category");
        if (params.get("id") != null) {
            categoryRepository.delete(Long.parseLong(params.get("id")));
        }
        return view;
    }

}
