package fr.eisti.fintzweb.controllers;

import fr.eisti.fintzweb.auth.AuthToken;
import fr.eisti.fintzweb.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by ragotpaul on 2/26/17.
 */

@Controller
public class VoteController {

    @Autowired
    CandidateRepository candidateRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    AssociationRepository associationRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    VoteRepository voteRepository;

    private Integer getEmployeeNumber(HttpSession session) {
        SecurityContextImpl context = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
        AuthToken token = (AuthToken) context.getAuthentication();
        return token.getUser().getEmployeeNumber();
    }

    private Iterable<Category> getCategories(Integer employeeNumber) {
        Iterable<Category> categories = categoryRepository.findAll();
        for (Category category :
                categories) {
            for (Vote vote :
                    voteRepository.findByCategory(category)) {
                if (vote.getVoter().getId().equals(userRepository.findByEmployeeNumber(employeeNumber).getId())) {
                    category.setUserVoteId(vote.getCandidate().getId());
                }
            }
        }
        return categories;
    }

    private Boolean isAlreadyVote(Category category, User user) {
        for (Vote vote :
                voteRepository.findByCategory(category)) {
            if (vote.getVoter().equals(user)) {
                return true;
            }
        }
        return false;
    }

    @GetMapping("/vote")
    public ModelAndView getVote(HttpServletRequest request) {
        ModelAndView view = new ModelAndView("vote");
        HttpSession session = request.getSession();
        Integer employeeNumber = this.getEmployeeNumber(session);
        view.addObject("categories", this.getCategories(employeeNumber));
        view.addObject("users", userRepository.findAllByOrderByLastnameAsc());
        view.addObject("families", familyRepository.findAll());
        view.addObject("associations", associationRepository.findAll());
        return view;
    }

    @PostMapping("/vote")
    public ModelAndView postVote(@RequestParam Map<String, String> params, HttpServletRequest request) {
        ModelAndView view = new ModelAndView("redirect:/vote");
        HttpSession session = request.getSession();
        Integer employeeNumber = this.getEmployeeNumber(session);
        for (Map.Entry<String, String> entry :
                params.entrySet()) {
            if (!entry.getKey().equals("_csrf")) {
                Candidate candidate = candidateRepository.findOne(Long.parseLong(entry.getValue()));
                Category category = categoryRepository.findOne(Long.parseLong(entry.getKey()));
                User voter = userRepository.findByEmployeeNumber(employeeNumber);
                if (!isAlreadyVote(category, voter)) {
                    voteRepository.save(new Vote(candidate, category, voter));
                } else {
                    voteRepository.setFixedCandidate(candidate, category, voter);
                }
            }
        }
        return view;
    }

}
