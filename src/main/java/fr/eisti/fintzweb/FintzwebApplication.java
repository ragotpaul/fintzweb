package fr.eisti.fintzweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FintzwebApplication {

    public static void main(String[] args) {
        SpringApplication.run(FintzwebApplication.class, args);
    }
}
