package fr.eisti.fintzweb.auth;

/**
 * Created by ragotpaul on 3/1/17.
 */
public class AuthResponse {

    private AuthUser user;

    private String status;

    public AuthResponse() {
    }

    public AuthResponse(AuthUser user, String status) {
        this.user = user;
        this.status = status;
    }

    public AuthUser getUser() {
        return user;
    }

    public void setUser(AuthUser user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isOK() {
        return this.status.equals("ok") ? true : false;
    }

}
