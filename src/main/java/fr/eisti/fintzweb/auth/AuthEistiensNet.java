package fr.eisti.fintzweb.auth;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ragotpaul on 3/1/17.
 */

public class AuthEistiensNet {

    public final static String URL = "https://auth.eistiens.net";

    public static AuthResponse getAuthResponse(String username, String password) throws Exception {
        AuthResponse response;
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> data = new LinkedMultiValueMap<String, String>();
        data.add("username", username);
        data.add("password", password);
        try {
            response = restTemplate.postForObject(URL, data, AuthResponse.class);
        } catch (Exception e) {
            response = null;
        }
        return response;
    }

}
