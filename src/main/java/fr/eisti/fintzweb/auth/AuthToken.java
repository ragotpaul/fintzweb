package fr.eisti.fintzweb.auth;

import fr.eisti.fintzweb.models.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by ragotpaul on 3/1/17.
 */
public class AuthToken extends UsernamePasswordAuthenticationToken {

    private User user;

    public AuthToken(Object principal, Object credentials, User user) {
        super(principal, credentials);
        this.user = user;
    }

    public AuthToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, User user) {
        super(principal, credentials, authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
