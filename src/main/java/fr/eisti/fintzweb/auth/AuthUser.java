package fr.eisti.fintzweb.auth;

/**
 * Created by ragotpaul on 3/1/17.
 */
public class AuthUser {

    private String last_name;

    private String first_name;

    private String email;

    private String employee_number;

    public AuthUser() {
    }

    public AuthUser(String last_name, String first_name, String email, String employee_number) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.email = email;
        this.employee_number = employee_number;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployee_number() {
        return employee_number;
    }

    public void setEmployee_number(String employee_number) {
        this.employee_number = employee_number;
    }
}
